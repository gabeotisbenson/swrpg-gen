const careers	= require('./careers');
const species = require('./species');

module.exports = {
	careers,
	species
};
