const mongoose	= require('mongoose');

const Schema = mongoose.Schema;

const specializationSchema = new Schema({
	'careerStub': {
		type: String,
		required: true,
		index: true
	},
	'stub': {
		type: String,
		required: true,
		index: true
	},
	'name': {
		type: String,
		required: true
	},
	'description': {
		type: String,
		required: true
	},
	'skills': [
		{
			'stub': {
				type: String,
				required: true
			},
			'name': String
		}
	]
});

const Specialization = mongoose.model('Specialization', specializationSchema);

module.exports = Specialization;
