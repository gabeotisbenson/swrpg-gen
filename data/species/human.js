const human = {
	name: 'Human',
	descriptions: [
		{
			game: 'Edge of the Empire',
			text: 'Humans are the most populous and gregarious of the galaxy\'s sapient species.  They seem to be present on almost every open planet that harbors life while traveling the spacelanes looking for more.'
		},
		{
			game: 'Age of Rebellion',
			text: 'Ubiquitous and dominant, humans are found throughout the galaxy.  The Empire believes humans to be the rightful rulers of all civilization; the Alliance feels differently, yet most of the leadership of even this egalitarian organization is made up of humans.  They are the least homogenous and most active species in known space.  Where they go, things happen.'
		},
		{
			game: 'Force and Destiny',
			text: 'Humans are the most common form of sentient life in the galaxy.  Characterized by an exceptional degree of adaptability, they have come to reside on most of the galaxy\'s habitable worlds.  Human cultures have explored much of the galaxy and established many colonies.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Humans are mammalian bipeds with mostly hairless skin.  Their skin tones encompass a gamut of colors, from peachy tones to dark brown and black.  Males average 1.8 meters tall, while females stand shorter and are generally not as stocky.  They process oxygen for respiration and can digest a variety of animal and vegetable matter.  Their biology is well-suited to many environments in the galaxy, which perhaps explains their dominance and ubiquity.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Averaging 1.8 meters in height (with females averaging slightly shorter), humans are mammalian bipeds with an internal biology that handles a wide variety of oxygenated environments well.  Their mostly hairless skin spans a wide range of color from very dark to very light; most range in the pale-tan to dark brown colors, but there are distinctive variants, especially among those offshoots considered near-human.  As omnivores, humans have a wide range of available food sources, as well.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Humans are oxygen breathing omnivores, capable of consuming a broad range of snimal and plant products for food.  A mammalian species, they have internal biology adaptable to a wide array of environmental conditions, and they tend to use clothing and technology to fu rther extend their ability to survive and thrive in all sorts of places.  Males average just under two meters inheight, while females are slightly shorter.  Human skin is mostly hairless, and comes in colors raning from off-white to very dark brown.  Hair color can vary even more wildly, running the gamut from shades of red and  yellow to solid black.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Human ambition and competition have split their species into many different societies.  Across the galaxy, humans live under every political system ever devised, from feudalism and theocracy to democracy and autocracy, with human technology encompassing a similarly wide variation.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Though the Empire would make it otherwise, humans are far too diverse and widespread throughout the galaxy to represent a single society.  Every form of government ever devised can be found in places where humans choose to live, and economic and sociological difference are just as varied.  Although humans helped advance technological developments throughout known space, human cultures living as true primatives exist as well.  Generally, humans yearn for independence and self-determination, yet there are countless billions who gladly subjugate themselves under tyrannical rule for the promise of prosperity and security.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Humans are far too broadly spread throughout the galaxy to constitute a single cultural entity.  Instead, the species embraces an extraordinary range of different cultural norms.  Humans live under virtually every known or imaginable political system.  Economic and socialogical attitudes vary substantially between places where humans dwell in peace and areas where they wage war with other sentient species or human factions.',
						'During the time of the Galactic Empire, humans are unquestionably the most powerful force in the galaxy.  They control the military and political forces of the Imperial government.  However, just as many humans work together against the Empire\'s tyranny.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Humans are thought to have arisen somewhere in the Core Worlds, perhaps on Coruscant itself.  However, humans took to the stars so long ago (perhaps even before the invention of faster-than-light travel) that now, humans claim countless planets as their homes.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Scholars continue to debate which of the Core Worlds actually stands as the true human homeworld; most claim it has to be Coruscant.  Others are convinced humans did not even originate in the Core, instead coming from far away on sleeper or generation ships long before the advent of hyperdrive technology.  Regardless, they now dominate the Core and are spread throughout the galaxy, claiming many worlds as their homes.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'The precise origin of human life has been lost to the ages.  Most scholars believe that the species originated from somewhere within the Core Worlds--arguably Coruscant.  However, humanity embraced star travel a very long time ago, founding colonies on every habitable world they could.  Human cultures now exist on countless different planets.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'The main language that connnects "baseline" humans is Basic, though each society has its own dialect and sometimes even sub-language.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Basic was originated by humans, and with their ubiquity came the widespread use of it as a connecting language for the entire galaxy.  Numerous human societies have variants and dialects, and there are entire human civilizations that have distinct languages with no connection to Basic at all.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Human cultures invented Basic, and many use it.  Some cultures, however, may speak different dialects of B asic, or entirely unique languages.'
					]
				}
			]
		},
		{
			title: 'Life on the Fringe',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Humans can be found in almost every role and working every job on the Fringe.'
					]
				}
			]
		},
		{
			title: 'Life in the Alliance',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'The inherent diversity of humans extends to their roles, careers, specializations, and chosen Duties with the Alliance.  The only constant is the lack of consistency where humans are concerned; they are likely to pursue any path and have any set of goals driving them.  The unifying factor is their desire to oppose the Empire, along with all of the other species who resist.'
					]
				}
			]
		},
		{
			title: 'Perceptions of the Force',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Humanity exhibits a broad range of attitudes toward the Force.  Some deny its existence completely, while others insist on seeing its influence upon all things.  Extreme attitudes can often exist together, even within the constraints of a single planet\'s culture.  Currently, the Galactic Empire\'s heavy-handed approach toward the Force informs most human opinions.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 2,
		agility: 2,
		intellect: 2,
		cunning: 2,
		willpower: 2,
		presence: 2
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'Brawn'
		},
		strain: {
			value: 10,
			ability: 'Willpower'
		}
	},
	baseXP: 110,
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Humans start the game with one rank in two different non-career skills of their choice.  They still may not train these skills above rank 2 at character creation.'
		}
	],
	extra: {
		humanityDuringTheEmpire: 'In the Empire, humans control all the levels of power in the galaxy--military, economic, and political.  Anti-alien prejudice is also at its height, and the Empire actively discriminates against non-humans.  However, not all humans have been brainwashed by anti-alien propaganda.  many show acts of kindness every day, and still others have taken up arms with their non-human brothers and sisters to resist the Empire.',
		aGalaxyOfHumans: 'Even in a galaxy of countless stars and planets containing potentially millions of species, one can find humans just about everywhere.  Some say humans were the first explorers to venture ou t into the darkness of space before the dawn of the Republic.  They say these early humans were bold explorers who colonized the rest of the galaxy while most species were still striking flint to rock.  Others, notably early spacefaring pioneers like the Duros, claim humanity simply has a penchant for expanding as fast as possible and sticking its collective nose where it doesn\'t belong.'
	}
};

module.exports = human;
