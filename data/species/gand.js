const gand = {
	name: 'Gand',
	descriptions: [
		{
			game: 'Edge of the Empire',
			text: 'Gands are a mysterious insectoid species whose "findsmen" treat tracking quarry as a religious duty.  They are exceptionally good at diving the location of individuals through ritualistic methods that off-worlders might consider backward and barbaric.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Though prevalent in the fringes of the Outer Rim, Gands remain a puzzle to xenobiologists.  Over a dozen distinct subspecies of the Gands have been reported.  All are covered in a dense chitinous exoskeleton and reach a height at adulthood of about 1.6 meters.  They have three fingers on each hand, which are as dextrous as those of other humanoids, allowing them to manipulate equipment designed for five-fingered individuals.',
						'Where Gands subspecies differ from one another is in their respiratory biology.  Most Gands who travel the spacelanes do not seem to need to breathe at all.  The digestion of food in their stomachs produce the ammonia necessary for their biochemistry.  Wasteful byproducts and extraneous ammonia are expelled through their exoskeletons.  These Gands often emanate an unpleasant methane musk.  Other Gand subspecies possess actual lungs and cannot produce ammonia through digestion, instead requiring a special breathing apparatus.',
						'Questions about Gand biology continue to go unanswered, since the Gand refuse to permit other species to study them.  Some xenobiologists conjecture that Gand religious rituals dictate genetic manipulation, which has produced many different varieties of Gands.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Gands have a closed culture.  What is known about them is that religion and life are closely entwined.  A gathering of elders called the Ruetsavii observes the various religious sects that represent the virtues of Gand life.  Off-worlders are most familiar with the religious sect known as the "findsmen."  These are the Gand who leave their home planet on ritualistic hunts to catch prey and bring divine blessings to their people.  Many other sects are rumored to exist, particularly in the fields of genetics, medicine, and engineering, but their exact natures are unknown.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Sharing the same name as its species, Gand is an Outer Rim world clouded in ammonia and methane.  Trade is handled through orbiting stations as foreigners are rarely allowed on the planet\'s surface.  Those non-Gand who land on the world must stay in the specially equipped Alien Quarters.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Droids are much better than well-trained humanoid linguists in reproducing the clicks and chirrups of the Gand spoken language.  Without the need to intake breathable gases, most Gand lack the vocal cords to make the sounds of Basic and other languages.  They must use droids or translation devices to converse in groups that do not understand their language.',
						'The grammar that Gand use reflects their place in society.  Before a Gand earns his identity and is accepted among his peers, he refers to himself in the third person, calling himself "Gand."  Once he has done a deed of distinction, a Gand may take his family name.  Gand cannot use their first names until they have become "persons of greatness"--janwuine in their own tongue--and received commendations from their peers.  This causes many off-worlders to view the Gand as a people of extreme humility.'
					]
				}
			]
		},
		{
			title: 'Life on the Fringe',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Since Gand findsmen have such a high success rate of catching targets, employers are perfectly willing to ignore cultural mysteries and odd smells.  Gand findsmen are in constant demand as bounty hunters, private investigators, assassins, skip tracers, and security advisers.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 2,
		agility: 2,
		intellect: 2,
		cunning: 2,
		willpower: 3,
		presence: 1
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 10,
			ability: 'willpower'
		}
	},
	baseXP: 100,
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Gands begin the game with one rank in Discipline.  They still may not train Discipline above rank 2 during character creation.'
		},
		{
			title: 'Ammonia Breathers',
			description: 'One notable difference between the two main sub-species of Gand is that one has lungs and one does not.  Those that have lungs breathe an ammonia gas mixture.  Those without lungs do not respire and gain all necessary metabolic substances through food.  When playing a Gand, each player chooses whether he wishes his character to have lungs or not.  If he selects to be playing a lungless Gand, his character is immune to suffocation (but not the wounds suffered from being exposed to vacuum).  If he chooses to play a Gand with lungs, he starts the game with an ammonia respirator, and treats oxygen as a dangerous atmosphere with Rating 8.  However, he gains +10 starting XP.'
		}
	],
	extra: {
		findsmen: 'Gand findsmen have become legends in the galaxy for their talents at tracking and hunting targets.  Findsmen invoke mystical mantras and look for omens in the most ordinary of places--pools of fuel, transparisteel smudges, the patterns of circuitry--to divine the location and intention of the one they seek.  These religious rituals findsmen call "the path to truth" appear bizarre to non-Gand, but no one will dispute a Gand findsman\'s end result.'
	}
};

module.exports = gand;
