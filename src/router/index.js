import CareerDetail		from '@src/views/pages/CareerDetail';
import CareerList			from '@src/views/pages/CareerList';
import Home						from '@src/views/pages/Home';
import Router					from 'vue-router';
import SpeciesDetail	from '@src/views/pages/SpeciesDetail';
import SpeciesList		from '@src/views/pages/SpeciesList';
import Vue						from 'vue';

Vue.use(Router);

export default new Router({
	routes: [
		{	path: '/', name: 'Home', component: Home },
		{ path: '/careers', name: 'Career List', component: CareerList },
		{ path: '/careers/:id', name: 'Career Detail', component: CareerDetail },
		{ path: '/species', name: 'Species List', component: SpeciesList },
		{ path: '/species/:id', name: 'Species Detail', component: SpeciesDetail }
	]
});
