// Required modules
const bluebird	= require('bluebird');
const express		= require('express');
const mongoose	= require('mongoose');

// Create the router
const router = express.Router();

// Connect to the DB
mongoose.connect('mongodb://localhost');
mongoose.Promise = bluebird;

// Pull in our models
const Character = require('../../models/character.js');

// List request
router.get('/', (req, res) => {
	Character.find({}, (err, docs) => {
		if (err && !docs) {
			res.send({
				success: false
			});
		} else {
			res.send({
				success: true,
				results: docs
			});
		}
	});
});

module.exports = router;
