// Required modules
const bluebird	= require('bluebird');
const express		= require('express');
const mongoose	= require('mongoose');

// Create the router
const router = express.Router();

// Connect to the DB
mongoose.connect('mongodb://localhost/swrpg-gen');
mongoose.Promise = bluebird;

// Pull in our models
const Species = require('../../models/species.js');

// List request
router.get('/', (req, res) => {
	Species.find({}, (err, docs) => {
		if (err && !docs) {
			res.send({
				success: false
			});
		} else {
			res.send({
				success: true,
				results: docs
			});
		}
	});
});

router.post('/', (req, res) => {
	const newSpecies = new Species(req.body);
	newSpecies.save((err) => {
		res.send({
			success: err !== null
		});
	});
});

module.exports = router;
