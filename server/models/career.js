const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const careerSchema = new Schema({
	'slug': {
		type: String,
		required: true,
		index: true
	},
	'name': {
		type: String,
		required: true
	},
	'skills': [
		{
			'stub': {
				type: String,
				required: true
			},
			'name': String
		}
	],
	'forceRating': {
		type: Number,
		required: true,
		default: 0
	},
	'description': {
		type: String,
		required: true
	}
});

const Career = mongoose.model('Career', careerSchema);

module.exports = Career;
