const gadgeteerTalentTree = {
	brace: {
		name: 'Brace',
		description: {
			full: 'Perform the Brace maneuver to remove one setback die per rank of Brace from next Action.  This may only remove a setback die added by environmental circumstances.',
			short: '-1 Setback Die'
		},
		active: true,
		ranks: {
			1: {
				cost: 5,
				root: true
			}
		}
	},
	toughened: {
		name: 'Toughened',
		description: {
			full: 'Gain +2 wound threshold.',
			short: '+2 Wound Threshold'
		},
		active: false,
		ranks: {
			1: {
				cost: 5,
				root: true,
				children: [
					{
						name: 'Jury Rigged',
						rank: 1
					}
				]
			},
			2: {
				cost: 15
			}
		},
		add (character) {
			character.thresholds.wound += 2;
			return character;
		},
		remove (character) {
			character.thresholds.wound += 2;
			return character;
		}
	},
	intimidating: {
		name: 'Intimidating',
		description: {
			full: 'May suffer a number of strain to downgrade difficulty of Coercion checks, or upgrade difficulty when targeted by Coercion checks, by an equal number.  Strain suffered this way cannot exceed ranks in Intimidating.',
			short: 'Suffer strain to modify difficulty of Coercion Checks'
		},
		active: true,
		ranks: {
			1: {
				cost: 5,
				root: true
			},
			2: {
				cost: 25
			}
		}
	},
	defensiveStance: {
		name: 'Defensive Stance',
		description: {
			full: 'Once per round, may perform Defensive Stance maneuver and suffer a number of strain to upgrade difficulty of all incoming melee attacks by an equal number.  Strain suffered this way cannot exceed ranks in Defensive Stance.',
			short: 'Suffer strain to reduce damage from melee attacks.'
		},
		active: true,
		ranks: {
			1: {
				cost: 5,
				root: true,
				children: [
					{
						name: 'Disorient',
						rank: 1
					}
				]
			}
		}
	},
	spareClip: {
		name: 'Spare Clip',
		description: {
			full: 'Cannot run out of ammo due to Despair.  Items with Limited Ammo quality run out of ammo as normal.',
			short: 'Cannot run out of ammo due to Despair.'
		},
		ranks: {
			1: {
				cost: 10
			}
		}
	},
	juryRigged: {
		name: 'Jury Rigged',
		description: {
			full: 'Choose 1 weapon, armor, or other item and give it a permanent improvement while it remains in use.',
			short: '+1 Permanent Improvement to gear.'
		},
		ranks: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Spare Clip',
						rank: 1
					},
					{
						name: 'Armor Master',
						rank: 1
					},
					{
						name: 'Point Blank',
						rank: 1
					}
				]
			},
			2: {
				cost: 20
			}
		}
	},
	pointBlank: {
		name: 'Point Blank',
		description: {
			full: 'Add 1 damage per rank of Point Blank to damage of one hit of successful attack while using Ranged (Heavy) or Ranged (Light) skills at close range or engaged.',
			short: '+1 damage when using Ranged weapons in close range or engaged.'
		},
		ranks: {
			1: {
				cost: 10
			}
		}
	},
	disorient: {
		name: 'Disorient',
		description: {
			full: 'After hitting with combat check, may spend 2 Advantage to disorient target for number of rounds equal to ranks in Disorient.',
			short: 'Spend 2 Advantage to disorient target.'
		},
		ranks: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Stunning Blow',
						rank: 1
					}
				]
			}
		}
	},
	armorMaster: {
		name: 'Armor Master',
		description: {
			full: 'When wearing armor, increase total soak value by 1.',
			short: '+1 Soak when wearing armor.'
		},
		ranks: {
			1: {
				cost: 15,
				children: [
					{
						name: 'Toughened',
						rank: 2
					},
					{
						name: 'Tinkerer',
						rank: 1
					},
					{
						name: 'Natural Enforcer',
						rank: 1
					}
				]
			}
		}
	},
	naturalEnforcer: {
		name: 'Natural Enforcer',
		description: {
			full: 'Once per session, may re-roll any 1 Coercion or Streetwise check',
			short: 'Re-roll 1 Coercion or Streetwise check.'
		},
		active: true,
		ranks: {
			1: {
				cost: 15
			}
		}
	},
	stunningBlow: {
		name: 'Stunning Blow',
		description: {
			full: 'When making Melee checks, may inflict damage as strain instead of wounds.  This does not ignore soak.',
			short: 'Melee checks may inflict damage as strain instead of wounds.'
		},
		active: true,
		ranks: {
			1: {
				cost: 15,
				children: [
					{
						name: 'Improved Stunning Blow',
						rank: 1
					}
				]
			}
		}
	},
	tinkerer: {
		name: 'Tinkerer',
		description: {
			full: 'May add 1 additional hard point to a number of items equal to ranks in Tinkerer.  Each item may only be modified once.',
			short: '+1 hard point to an item.'
		},
		ranks: {
			1: {
				cost: 20,
				children: [
					{
						name: 'Jury Rigged',
						rank: 2
					},
					{
						name: 'Dedication',
						rank: 1
					},
					{
						name: 'Deadly Accuracy',
						rank: 1
					}
				]
			}
		}
	},
	deadlyAccuracy: {
		name: 'Deadly Accuracy',
		description: {
			full: 'When acquired, choose 1 combat skill.  Add damage equal to ranks in that skill to one hit of successful attack made using that skill.',
			short: 'Add damage equal to ranks in a skill of your choice to a successful attack.'
		},
		ranks: {
			1: {
				cost: 20
			}
		}
	},
	improvedStunningBlow: {
		name: 'Improved Stunning Blow',
		description: {
			full: 'When dealing strain damage with Melee or Brawl checks, may spend Triumph to stagger target for 1 round per Triumph.',
			short: 'Spend Triumph to cause target to stagger.'
		},
		active: true,
		ranks: {
			1: {
				cost: 20,
				children: [
					{
						name: 'Crippling Blow',
						rank: 1
					}
				]
			}
		}
	},
	dedication: {
		name: 'Dedication',
		description: {
			full: 'Gain +1 to a single chracteristic.  This cannot bring a characteristic above 6.',
			short: '+1 to a characteristic of your choice.'
		},
		ranks: {
			1: {
				cost: 25,
				children: [
					{
						name: 'Intimidating',
						rank: 2
					},
					{
						name: 'Improved Armor Master',
						rank: 1
					}
				]
			}
		}
	},
	improvedArmorMaster: {
		name: 'Improved Armor Master',
		description: {
			full: 'When wearing armor with a soak value of 2 or higher, increase defense by 1.',
			short: '+1 Defense when wearing armor with Soak Value >= 2'
		},
		ranks: {
			1: {
				cost: 25
			}
		}
	},
	cripplingBlow: {
		name: 'Crippling Blow',
		description: {
			full: 'Increase the difficulty of next combat check by 1.  If check deals damage, target suffers 1 strain whenever he moves for the remainder of the encounter.',
			short: 'Increase difficulty of next combat check to cause target to suffer 1 strain upon movement.'
		},
		ranks: {
			1: {
				cost: 25
			}
		}
	}
};

module.exports = gadgeteerTalentTree;
