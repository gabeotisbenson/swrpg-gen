const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const talentSchema = new Schema({
	'stub': {
		type: String,
		required: true,
		index: true
	},
	'name': {
		type: String,
		required: true
	},
	'description': {
		type: String,
		required: true
	},
	'cost': {
		type: Number,
		required: true
	},
	'isForce': {
		type: Boolean,
		required: true,
		default: false
	},
	'depends': [String]
});

module.exports = talentSchema;
