/* global XMLHttpRequest */
const request = (method, url, callback) => {
	const r = new XMLHttpRequest();
	r.open(method, url, true);

	r.onload = () => {
		if (r.status >= 200 && r.status < 400) {
			const data = JSON.parse(r.response);
			callback(null, data);
		} else {
			callback(new Error('Server returned an error: ' + r.status));
		}
	};

	r.onerror = () => {
		callback(new Error('Connection could not be established.'));
	};

	r.send();
};

const get = (url, callback) => {
	request('GET', url, callback);
};

const put = (url, callback) => {
	request('PUT', url, callback);
};

const post = (url, callback) => {
	request('POST', url, callback);
};

const del = (url, callback) => {
	request('DELETE', url, callback);
};

export default {
	get,
	put,
	post,
	del
};
