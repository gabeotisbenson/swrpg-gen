const droid = {
	name: 'Droid',
	descriptions: [
		{
			game: 'Edge of the Empire',
			text: 'A typical fringer may ridicule his droid to no end, yet without it, he is usually lost.  Droids serve a variety of roles on the fringe, including security and conflict resolution, surgery and medicine, mechanical repairs, construction, astrogation and piloting, and mechanical labor.  While many of these droids have only rudimentary intelligence, some are capable of independent thought.'
		}
	],
	variations: [
		{
			name: '2-1B',
			manufacturer: 'Genetech/Industrial Automaton',
			category: 'Surgical Droid',
			description: [
				'The 2-1B Surgical Droid is a highly intelligent and flexible thinker; as one would expect of a droid whose decisions involve life and death.  Its rather ugly chassis hides a remarkably complex and flexible behavioral circuitry matrix, and the droids are intelligent and capable in equal measure.'
			]
		},
		{
			name: '3PO',
			manufacturer: 'Cybot Galactica',
			category: 'Protocol Droid',
			description: [
				'The 3PO series of protocol droids are some of the most human-like automata ever developed--perfect for their assigned tasks as ambassadors, political aids, translators, and personal attaches.  However, their advanced SyntheTech AA-1 verbobrains can cometimes develop neuroses or other "quirks".'
			]
		},
		{
			name: 'IG',
			category: 'Assassin Droid',
			manufacturer: 'Holowan Mechanical',
			description: 'Culminating with the feared and deadly IG-88 model, the IG series is infamous as one of the most lethal assassin droids in the galaxy.  IG droids rarely meet an opponent they cannot eliminate, and true to their humorless nature, they follow the instructions for a particular bounty to the letter.'
		},
		{
			name: 'LE-V0',
			category: 'Law Enforcement Droid',
			manufacturer: 'Rseikharhl Droid Group',
			description: 'The success of LE-V0s in deterring crime in the Rseikharhl sector has led to their adoption galaxy-wide as appropriate models for police assistance and private security.  Their programming requires them to follow Imperial codes to the letter and only use force when absolutely necessary.  Though they are hardly ever equipped with lethal weaponry, they know how to target a stun weapon for maximum effect.'
		},
		{
			name: 'LOM',
			category: 'Protocol Droid',
			manufacturer: 'Industrial Automaton',
			description: 'The LOM series was Industrial Automaton\'s attempt to upset a market dominated by Cybot Galactica.  IA attempted to make a protocol droid very similar to the 3PO unites but with an insectoid head to appeal to niche markets.  However, bad PR from at least one unit\'s career as a jewel theief caused IA to scrap the line.  Some of the remaining LOM droids do have a habit of developing interesting and unexpected personalities.'
		},
		{
			name: 'R-Series',
			category: 'Astromech Droid',
			manufacturer: 'Industrial Automaton',
			description: 'The wildly successful R-series ranges from the original and cumbersome R1 to the famous R2 unit and all the way to the soon-to-be-released R6.  Aside from the R1 (which is more than two meters tall and works aboard capital ships), all of the R-series astromechs are roughly a meter or so tall, and designed to ride in sockets aboard snubfighters tto perform astrogation calculations and allow hyperspace jumps.  A wide variety of tools also make them decent repair droids.'
		}
	],
	information: [
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Smugglers have long traded rumors of "droid worlds," where automatons and cyborgs govern a society of their own.  Aside from such tall tales, droids do not have their own society, instead laboring within the society of their human and non-human masters.  Unless a droid\'s personality has reprogrammed its cognitive faculties, droids are built to obey, and they do.',
						'However, the droids played by PCs present a special case.  Over the millennia there have been countless examples of droids that have transcended their original programming to become self-aware and self operating.  Some of these automata last for hundreds of years, repairing and upgrading themselves to remain at peak efficiency.',
						'Some of these droids take pride and satisfaction in continuing to do the jobs they were designed for as well as possible, such as an R2 unit that constantly works to become a better pilot and astrogator.  Others break with their programming entirely and choose new enterprises to pursue.  The infamous 4-LOM started out life as aprotocol droid, before becoming a jewel thief and eventually one of the galaxy\'s most well-known bounty hunters.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'In the wider galaxy (aside from wild rumors about "machine planets"), most droids are mere functionaries within societies that completely rely upon them and expect them to obey.  Within the Alliance, however, they have come to know a higher level of respect.  Though many in the Alliance\'s structure still hold with the idea of memory wipes and programming for necessary tasks only (especially when droids are acquired from Imperial or questionable sources), more and more leaders and commanders have taken the long view on allowing trusted droids to continue to develop a sense of personal independence and purpose.  On more than one occasion, this has proven to be a brilliant strategy, and will likely continue.',
						'As such, many droids have transcended their programming and original purpose to become mroe than they started as, and find service in the Alliance very satisfactory.  Granted, those coming from outside regular channels face a great deal of suspicion, which is often impossible to overcome.  Technicians usually scan a droid\'s memory bank and programming thoroughly to ensure there are no hidden commands betraying the Rebellion in any way.'
					]
				}
			]
		},
		{
			title: 'Life on the Fringe',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Droids from all classes operate with smugglers and criminal groups in the Outer Rim, where there are those who are willing to overlook their mechanical nature and respect a fellow fringer with a valuable skill set.  Some bear their original design without modification, while other units are cleverly disguised in other bodies, such as the Cybot Galactica protocol series.  And there are a handful of droids like the pirate lord Z1-Z0 who have taken business into their own hands, finding it more efficient to lead than serve.'
					]
				}
			]
		},
		{
			title: 'Life in the Alliance',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'Most droids are better suited to non-combat roles, serving in support areas to better enable the forces of the Rebellion to fight efficiently and effectively.  Many droids of a technical nature serve aboard capital ships, often handling the tasks of dozens of non-droid crew, making personnel shortages less devastating to the Alliance Navy.  Astromech droids abound as more and more sunbfighters, like the X-wing, are brought into service.',
						'Individual droids with their own motivations can serve in any capacity they desire, though most choose careers and specializations that fit their basic design and functions.  The Duty a droid chooses, as well, should suit its chosen path.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 1,
		agility: 1,
		intellect: 1,
		cunning: 1,
		willpower: 1,
		presence: 1
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 10,
			ability: 'willpower'
		}
	},
	baseXP: 175,
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Droids do not need to eat, sleep, or breathe, and are unaffected by toxins or poisons.  Droids have a cybernetic implant cap of 6 instead of their Brawn rating.  In addition, after selecting their career, a Droid Player Character may train one rank in six of the eight career skills (instead of the usual four).  After selecting their first specialization, a Droid Player Character may train one rank in three of the four specialization skills (instead of the usual two).'
		},
		{
			title: 'Inorganic',
			description: 'Since droids are inorganic, they do not gain the benefits of recovering with a bacta tank, stimpack, or Medicine skill checks.  Droids do recover naturally by resting, as their systems attempt self-repairs.  Otherwise, droids need to be tended to with a Mechanic\'s check, using the same difficulties and results of Medicine checks for organic beings.  Emergency repair patches can be used to repair damage just like stimpacks are used on organic beings.  See page 220 for more on droid repairs and healing.  Due to their resilient metallic construction, droids start the game with one rank in the Enduring talent.'
		},
		{
			title: 'Mechanical Being',
			description: 'Droids cannot become Force sensitive, nor acquire a Force Rating by any means.  Droids cannot use Force powers, and also cannot be affected by mind-altering Force powers.'
		}
	],
	extra: {
		classifications: {
			description: 'Droids manufacturers organize droids in five distinct classes based on their primary functions.',
			one: {
				degree: 'First',
				description: 'Specializing in physical sciences, mathematics, and medicine, these droids are often highly intelligent intellectuals, but lacking in "common sense."'
			},
			two: {
				degree: 'Second',
				description: 'Class Twos work in the engineering and technical fields such as repairs and astrogation.  They are often well-liked due to their resonable intelligence, non-threatening appearances, and quirky personalities.'
			},
			three: {
				degree: 'Third',
				description: 'Class Three droids are often humanoid in appearance, as they are intended to work directly with organics.  They are programmed for the social and service areas, such as interpretation, teaching, protocol, and diplomatic assistance.'
			},
			four: {
				degree: 'Fourth',
				description: 'Class Fours are equipped with weaponry and designed for security, military training and oeprations, gladitorial combat, and even assassination.'
			},
			five: {
				degree: 'Fifth',
				description: 'Class Fives are simple labor units for a whole host of menial jobs, from sanitation to load-lifting.  Many do not have enough cognition to be considered sentient.'
			}
		}
	},
	droidsAndEquipment: {
		edgeOfTheEmpire: 'Droids do not typically wear clothing, and many items of equipment that organics would have to carry separately may actually be part of a droid\'s body.  For this reason, droids are allowed to treat certain pieces of equipment differently than other characters.  For example, if a droid purchases and wears armor, the player can simply say his character has upgraded armor plates on his body, or a reinforced outer covering.  Likewise, he could have an upgraded verbobrain instead of a datapad, or an internal communications device instead of a comlink (although many droids do carry equipment such as datapads and comlinks).',
		ageOfRebellion: 'A Droid PC may declare as intrinsic to its construction virtually any piece of equipment an organic would need to carry.  The GM has final say on what is or is not allowed, but most such gear is permissible to treat as inherent to the droid\'s construction or to add to the droid as an upgrade when the gear is purchased.'
	}
};

module.exports = droid;
