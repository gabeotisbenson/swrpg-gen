// Required modules
const bluebird	= require('bluebird');
const models		= require('../server/models');
const mongoose	= require('mongoose');
const rl				= require('readline-sync');

// Connect to the DB
mongoose.connect('mongodb://localhost/swrpg-gen');
mongoose.Promise = bluebird;

const enterSpecies = (callback) => {
	// Set some static variables
	const abilities = [
		'Brawn',
		'Agility',
		'Intellect',
		'Cunning',
		'Willpower',
		'Presence'
	];
	const thresholds = [
		'Wound',
		'Strain'
	];
	console.log('\nSPECIES CREATION');
	// Create the initial species object
	const species = {
		name: rl.question('Name: '),
		information: [],
		abilities: [],
		thresholds: [],
		perks: []
	};
	// Information Sections
	// console.log('\nINFORMATION SECTIONS');
	// const numInfos = Number(rl.question('Number of info sections: '));
	// for (let i = 0; i < numInfos; ++i) {
	// 	console.log('Info Section ' + i);
	// 	species.information.push({
	// 		header: rl.question('Header: '),
	// 		description: rl.question('Description: ')
	// 	});
	// }
	// Abilities
	console.log('\nABILITIES');
	abilities.forEach((ability) => {
		species.abilities.push({
			name: ability,
			value: Number(rl.question(ability + ' Value: '))
		});
	});
	// Thresholds
	console.log('\nTHRESHOLDS');
	thresholds.forEach((threshold) => {
		species.thresholds.push({
			name: threshold,
			value: Number(rl.question(threshold + ' Threshold: ')),
			ability: rl.question('Dependent ability: ')
		});
	});
	// Experience
	console.log('\nEXPERIENCE');
	species.xp = Number(rl.question('Starting experience: '));
	// Perks
	console.log('\nPERKS');
	const numPerks = Number(rl.question('Number of perks: '));
	for (let i = 0; i < numPerks; ++i) {
		console.log('\nPerk Section ' + i);
		species.perks.push({
			name: rl.question('Name: '),
			description: rl.question('Description: ')
		});
	}
	// Prompt confirmation
	console.log('\nCONFIRMATION');
	console.log(species);
	if (/y(es)?/.test(rl.question('Does the above species look okay? ').toLowerCase())) {
		// Save the record
		console.log('Saving...');
		const newSpecies = new models.Species(species);
		newSpecies.save((err, doc) => {
			if (err) console.error(err);
			if (doc) console.log('Saved!');
			callback();
		});
	} else {
		enterSpecies();
	}
};

const main = () => {
	console.log('What would you like to do?');
	console.log('1: Enter a Species');
	console.log('q: Quit');
	const choice = rl.question('Choice: ');
	if (choice === '1') {
		enterSpecies(main);
	} else if (choice === 'q') {
		process.exit();
	}
};

main();
