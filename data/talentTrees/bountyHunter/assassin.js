const assassinTalentTree = {
	grit: {
		name: 'Grit',
		description: {
			full: 'Gain +1 strain threshold',
			short: 'Strain Threshold +1'
		},
		active: false,
		ranks: {
			1: {
				cost: 5,
				children: [
					{
						name: 'Precise Aim',
						rank: 1
					}
				],
				root: true
			}
		},
		add (character) {
			++character.thresholds.strain;
			return character;
		},
		remove (character) {
			--character.thresholds.strain;
			return character;
		}
	},
	lethalBlows: {
		name: 'Lethal Blows',
		description: {
			full: 'Add +10 per rank of Lethal Blows to any Critical Injury results inflicted on opponents.',
			short: 'Critical Injury +10'
		},
		active: false,
		ranks: {
			1: {
				cost: 5,
				children: [
					{
						name: 'Jump Up',
						rank: 1
					}
				],
				root: true
			},
			2: {
				cost: 15,
				children: [
					{
						name: 'Stalker',
						rank: 2
					},
					{
						name: 'Dodge',
						rank: 2
					}
				]
			},
			3: {
				cost: 20,
				children: [
					{
						name: 'Master of Shadows',
						rank: 1
					}
				]
			}
		}
	},
	stalker: {
		name: 'Stalker',
		description: {
			full: 'Add one boost die per rank of Stalker to all Stealth and Coordination checks.',
			short: '+1 Boost for Stealth, Coordination'
		},
		active: false,
		ranks: {
			1: {
				cost: 5,
				children: [
					{
						name: 'Quick Strike',
						rank: 1
					}
				],
				root: true
			},
			2: {
				cost: 20,
				children: [
					{
						name: 'Sniper Shot',
						rank: 1
					},
					{
						name: 'Precise Aim',
						rank: 2
					}
				]
			}
		}
	},
	dodge: {
		name: 'Dodge',
		description: {
			full: 'When targeted by combat check, may perform a Dodge incidental to suffer a number of strain no greater than ranks of Dodge, then upgrade the difficulty of the check by that number.',
			short: 'Suffer strain to increase difficulty check for attacker'
		},
		active: true,
		ranks: {
			1: {
				cost: 5,
				children: [
					{
						name: 'Quick Draw',
						rank: 1
					}
				],
				root: true
			},
			2: {
				cost: 20,
				children: [
					{
						name: 'Dedication',
						rank: 1
					}
				]
			}
		}
	},
	preciseAim: {
		name: 'Precise Aim',
		description: {
			full: 'Once per round, may perform Precise Aim maneuver.  Suffer a number of strain no greater than ranks in Precise Aim, then reduce target\'s melee and ranged defense by that number.',
			short: 'Suffer strain to reduce target\'s melee and ranged defense'
		},
		active: true,
		ranks: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Jump Up',
						rank: 1
					},
					{
						name: 'Targeted Blow',
						rank: 1
					}
				]
			},
			2: {
				cost: 25
			}
		}
	},
	jumpUp: {
		name: 'Jump Up',
		description: {
			full: 'Once per round, may stand from seated or prone as an incidental.',
			short: 'Stand up as incidental'
		},
		active: true,
		ranks: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Precise Aim',
						rank: 1
					},
					{
						name: 'Stalker',
						rank: 2
					},
					{
						name: 'Quick Strike',
						rank: 1
					}
				]
			}
		}
	},
	quickStrike: {
		name: 'Quick Strike',
		description: {
			full: 'Add one Boost die per rank of Quick Strike to combat checks against targets that have not acted yet this encounter.',
			short: '+1 Boost for first attack'
		},
		active: false,
		rank: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Jump Up',
						rank: 1
					},
					{
						name: 'Lethal Blows',
						rank: 2
					},
					{
						name: 'Quick Draw',
						rank: 1
					}
				]
			}
		}
	},
	quickDraw: {
		name: 'Quick Draw',
		description: {
			full: 'Once per round, draw or holster a weapon or accessible item as an incidental.',
			short: 'Draw/holster weapon as incidental'
		},
		active: true,
		ranks: {
			1: {
				cost: 10,
				children: [
					{
						name: 'Quick Strike',
						rank: 1
					},
					{
						name: 'Anatomy Lessons',
						rank: 1
					}
				]
			}
		}
	},
	targetedBlow: {
		name: 'Targeted Blow',
		description: {
			full: 'After making a successful attack, may spend 1 Destiny Point to add damage equal to Agility to one hit.'
		},
		active: true,
		ranks: {
			1: {
				cost: 15,
				children: [
					{
						name: 'Stalker',
						rank: 3
					}
				]
			}
		}
	},
	anatomyLessons: {
		name: 'Anatomy Lessons',
		description: {
			full: 'After making a successful attack, may spend 1 Destiny Point to add damage equal to intellect to one hit.'
		},
		active: true,
		ranks: {
			1: {
				cost: 15,
				children: [
					{
						name: 'Lethal Blows',
						rank: 3
					}
				]
			}
		}
	},
	sniperShot: {
		name: 'Sniper Shot',
		description: {
			full: 'Before making a non-thrown ranged attack, may perform a Sniper Shot maneuver to increase the weapon\'s range by 1 range band per rank in Sniper Shot.  Upgrade the difficulty of the attack by 1 per range band increase.'
		},
		active: true,
		ranks: {
			1: {
				cost: 20,
				children: [
					{
						name: 'Stalker',
						rank: 3
					},
					{
						name: 'Deadly Accuracy',
						rank: 1
					}
				]
			}
		}
	},
	deadlyAccuracy: {
		name: 'Deadly Accuracy',
		description: {
			full: 'When acquired, chose 1 combat skill.  Add damage equal to ranks in that skill to one hit of successful attack made using that skill.'
		},
		active: false,
		ranks: {
			1: {
				cost: 25
			}
		}
	},
	dedication: {
		name: 'Dedication',
		description: {
			full: 'Gain +1 to a single characteristic.  This cannot bring a characteristic above 6.',
			short: '+1 to any characteristic'
		},
		active: false,
		ranks: {
			1: {
				cost: 25
			}
		}
	},
	masterOfShadows: {
		name: 'Master of Shadows',
		description: {
			full: 'Once per round, suffer 2 strain to decrease difficulty of next Stealth or Skulduggery check by one.'
		},
		active: true,
		ranks: {
			1: {
				cost: 25
			}
		}
	}
};

module.exports = assassinTalentTree;
