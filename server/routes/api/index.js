const express	= require('express');
const multer	= require('multer');
const path		= require('path');

const router = express.Router();
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'dist/uploads/');
	},
	filename: (req, file, cb) => {
		cb(null, Date.now() + path.extname(file.originalname));
	}
});
const upload = multer({
	storage,
	limits: {
		fileSize: 5000000,
		files: 1
	}
});

router.get('/', (req, res) => {
	res.send('hey dude');
});

router.post('/image', upload.single('image'), (req, res) => {
	console.log(req.file);
});

const characterRoute = require('./character.js');
router.use('/character', characterRoute);
const speciesRoute = require('./species.js');
router.use('/species', speciesRoute);

module.exports = router;
