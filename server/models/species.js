const mongoose	= require('mongoose');

const Schema = mongoose.Schema;

const speciesSchema = new Schema({
	'name': {
		type: String,
		required: true
	},
	'image': String,
	'information': [
		{
			'header': {
				type: String,
				required: true
			},
			'description': {
				type: String,
				required: true
			}
		}
	],
	'abilities': [
		{
			'name': {
				type: String,
				required: true
			},
			'value': {
				type: Number,
				required: true
			}
		}
	],
	'thresholds': [
		{
			'name': {
				type: String,
				required: true
			},
			'value': {
				type: Number,
				required: true
			},
			'ability': {
				type: String,
				required: true
			}
		}
	],
	'xp': {
		type: Number,
		required: true
	},
	'perks': [
		{
			'name': {
				type: String,
				required: true
			},
			'description': {
				type: String,
				required: true
			}
		}
	]
});

const Species = mongoose.model('Species', speciesSchema);

module.exports = Species;
