const bothan			= require('./bothan');
const cerean			= require('./cerean');
const droid				= require('./droid');
const duros				= require('./duros');
const gand				= require('./gand');
const gran				= require('./gran');
const human				= require('./human');
const ithorian		= require('./ithorian');
const kelDor			= require('./kelDor');
const mirialan		= require('./mirialan');
const monCalamari	= require('./monCalamari');
const nautolan		= require('./nautolan');
const rodian			= require('./rodian');
const sullustan		= require('./sullustan');
const togruta			= require('./togruta');
const trandoshan	= require('./trandoshan');
const twilek			= require('./twilek');
const wookiee			= require('./wookiee');
const zabrak			= require('./zabrak');

module.exports = {
	bothan,
	cerean,
	droid,
	duros,
	gand,
	gran,
	human,
	ithorian,
	kelDor,
	mirialan,
	monCalamari,
	nautolan,
	rodian,
	sullustan,
	togruta,
	trandoshan,
	twilek,
	wookiee,
	zabrak
};
