const gran = {
	name: 'Gran',
	descriptions: [
		{
			game: 'Age of Rebellion',
			text: 'Keen of sight, inherently peaceful and nature-loving, and intrinsically in need of social contact, the Gran are easily abused by the policies of the violent and careless Empire.  This has driven even the most pacifistic to seek fellowship and service with the Rebellion.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'Descended from mammalian herbivores, Gran are bipedal humanoids with an appearance made very distinctive by their three stalked eyes.  Their snout-shaped faces resemble those of goats, and they tend toward having larger hands and feet.  Some even suffer from a genetic mutation causing abnormal enlargement of the extremeties, though this tends to have more social than practical effects on the sufferer\'s life.',
						'Gran eyesight extends into much greater ranges than that of most humanoids, even reaching into the infrared spectrum.  A Gran is capable of a certain kind of empathy with others of his species, reading their body heat and color changes to determine their emotional state.  Those Gran who spend considerable time with another species eventually learn how to do this with that species, as well.',
						'Gran have two stomachs, and they are able to store and digest food over a long period of time.  They usually have meals lasting for the better part of a day if they are able; if this happens, a Gran can normally go for anywhere from three to five days comfortably without another meal, depending on the nutritional value of the food.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'Peace and unity has always defined the foundation of Gran civilization, starting from the herd nature of their earliest days.  They unite in close-knit communities, staying close to extended families and friends, and they endeavor to live in harmony with their bucolic agricultural environment.  Family elders have strong leadership roles, and gatherings of elders form the government administering the planet and its people.',
						'Most Gran grow up knowing they will serve their community in a capacity chosen for them, based on their demonstrated skills and talents.  Though abhorrent to most other species, this flows naturally from the very communal nature of Gran society, and the majority of the population embraces it with contentment.',
						'A Gran\'s need for social contact can be a devastating hindrance in the wrong circumstances; the gravest form of punishment in the society is that of exile.  Many Gran encountered out among the stars wear black, to mourn the loss of their families and cherished relationships, as well as to try to forget the bright colors of their homes.  Such Gran fall readily to temptation and manipulation from those who would abuse them; a Gran in exile can become a bitter and dangerous foe.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'The Gran hail from the planet of Kinyen, a beautiful and productive agriworld in the Expansion Region.  Sadly, after the destruction of one of their cities, the Gran have been forced to capitulate utterly to the Empire.  They also have colonies on Hok and Malastare, which have much more diverse natures within their cultures; Malastare, in particular, is known for being a far more corrupt and dangerous place.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'The spoken and written tongue among Gran is called Gran.  Non-native speakers have difficulty conveying themselves effectively with most Gran due to the lack of compatible body temperature and color cues.  More cosmopolitan Gran can generally overcome this problem.'
					]
				}
			]
		},
		{
			title: 'Life in the Alliance',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'The vast majority of Gran serving the Rebellion seek out non-combat roles to fulfill, eschewing violence completely.  However, a few are able to see the need to sacrifice their homeworld\'s core principles in order to free it from the boot heel of Imperial occupation.  With their extraordinary visual sense and empathic abilities, Gran are often found with Duties focused on Intelligence, Personnel, Political Support, and Recruiting; they are best suited to Diplomat and Spy careers, as well as the Medic specialization in the Soldier career.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 2,
		agility: 2,
		intellect: 2,
		cunning: 1,
		willpower: 2,
		presence: 3
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 9,
			ability: 'willpower'
		}
	},
	baseXP: 100,
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Gran begin the game with one rank in Charm or Negotiation.  They still may not train Charm or Negotiation above rank 2 during character creation.'
		},
		{
			title: 'Enhanced Vision',
			description: 'When making ranged combat or Perception checks, Gran remove up to [][] imposed due to environmental conditions or concealment (but not defense).'
		}
	]
};

module.exports = gran;
