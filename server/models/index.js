// Import models
const Career	= require('./career');
const Character	= require('./character');
const Specialization	= require('./specialization');
const Species					= require('./species');
const TalentTree			= require('./talentTree');
const User						= require('./user');

module.exports = {
	Career,
	Character,
	Specialization,
	Species,
	TalentTree,
	User
};
