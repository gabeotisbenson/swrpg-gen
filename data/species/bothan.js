const bothan = {
	name: 'Bothan',
	descriptions: [
		{
			game: 'Edge of the Empire',
			text: 'Bothans are the galaxy\'s information brokers.  Adept at picking up on secrets or seeing things other species ignore, Bothans can be valuable assets in any endeavor--or untrustworthy partners.'
		},
		{
			game: 'Age of Rebellion',
			text: 'Masters of manipulation and collectors of data, Bothans are known to excel both as spies and as displomats.  Though it is berhaps unfair to universally characterize them as untrustworthy, it is generally unwise to forget that most Bothans are raised believing their own interests are genuinely paramount.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Bothans have highly attuned senses which aid in gathering and relaying information.  The noses at the end of their tapered snouts can distinguish many smells, and their eyes have an extended range of focus.  They possess ears double the size of most species and can rotate and bend them to pick up directional sounds.  Standing on average 1.6 meters, their small size allows Bothans to often go unnocited and slip through cracks that would never fit a human.  Their greatest asset is their fur.  They can communicate with other Bothans by ripping their fur, demonstrating changes in mood and conveying more specific intimations in a code.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Averaging 1.5 to 1.6 meters tall, Bothans are bipedal humanois taht, from a human\'s perspective, display an interesting mix of feline, quine, and canine traits.  With multidirectional and oversized ears, keen olfactory senses contained in their tapered snouts, and eyes capable of a much wider range of focus than the average humanoid, Bothans are extremely effective at picking up on the vast diversity of information they instinctively crave.  Thanks to their diminuitive stature, they are capable of getting into places barred to human-sized (and larger) beings.  Bothans also possess a unique manner of communication that involves rippling their fur in patterns decipherable by other Bothans.  Though capable of wearing footgear, Bothans have hooves instead of feet and both males and females sport a dangling beard at the end of their snout.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Every layer of Bothan society is based on the acquisition, manipulation, and utilization of knowledge.  Sometimes likened to the Hutts and Twi\'leks, Bothans take such comparisons as an insult.  Prestige, not wealth, motivates them.  A Bothan\'s reputation is, at the end of the day, all he has.  The bothan Council governs the Bothan worlds.  The major clans all have one representatives in the Council, which then elects the Council Chief.  Bothans shun outright confrontation.', 'Information is their weapon, and Bothans play secrets like a Corellian plays sabacc: the truth is the ace up their sleeve.',
						'The Bothans operate the most elaborate and extensive intelligence network across the galaxy.  This "spynet" works through moles, provocateurs, sleepers, and drop-offs which transfer information down a clandestine web to the Bothan spymasters.  Though the spynet hubs on Bothawui are Bothan-run, the majority of its "placed" agents are foreign species.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Though the planet of Bothawui is officially neutral in the conflict between the Empire and the Alliance (and, in fact, there are plenty of Bothans who play both sides for their personal gain), the upper echelons of the Alliance know the galaxy-spanning Bothan Spynet is working actively to further Alliance interests.  Nonetheless, the leadership of Bothawui plays the game with both sides very carefully, and somehow it works; both Alliance and Imperial intelligence operations use the planet for clandestine purposes and keep it out of the line of fire in the conflict.',
						'Despite reputations for being as greedy and devious as Hutts, Bothans are, for the most part, ethical beings.  Their ethics, however, derive from a code of behavior they call "The Way," which teaches that the pursuit of power and influence is right and necessary.  For Bothans, money is only a means, not an end; ultimately, the capacity to influence matters to reflect their goals is the prize which must be sought.  Bothans may well select a cause--a noble one, at that--to champion.  They simply prefer manipulation and scheming to gain victory for the cause, rather than resorting to violence.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Bothans hail from Bothawui, a temperate world in the Mid Rim.  They have also established major colonies on Kothlis and Torolis.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'Bothawui is a Mid-Rim world, temperate in climate and varied in terrain.  Cosmopolitan and developed, Bothawui is rich in natural resources, agriculture, and industry.  It is also the center of a Bothan-dominated sector of planets.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Bothese is the native tongue of the Bothans and Botha is its written form.  Almost all Bothans are fully fluent in Basic.  Wrendui, which in Bothese means the nonverbal cues and emotions a Bothan can communicate through his fur, has many coded forms that a spectator can translate into explicit information if the cipher is known.  Well-trained Bothan spies can switch between different forms of wrendui in a single ripple of their fur.'
					]
				},
				{
					game: 'Age of Rebellion',
					sections: [
						'The spoken form of the Bothans\' language is Bothese, and they refer to their written tongue as Botha.  They have a third form of communication called Wrendui, which is based solely on the rippling of their fur.  Though the base use of such ripples merely conveys emotions, they have developed many forms of Wrendui (which can be switched at will with the simplest of cues) to communicate in far more complex ways.  Highly placed non-Bothans in the Bothan Spynet are taught a few of these forms, but only a Bothan may know all of them.'
					]
				}
			]
		},
		{
			title: 'Life on the Fringe',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Bothans gravitate towards professions in which information is king.  They make strong Traders, Scouts, Thieves, and Politicos.  They will attempt to ascend the chain of command of their current career and will also funnel knowledge to the Bothan Spy Network to help their clan back on Bothawui.'
					]
				}
			]
		},
		{
			title: 'Life in the Alliance',
			descriptions: [
				{
					game: 'Age of Rebellion',
					sections: [
						'Not surprisingly, Bothans are both drawn to and encouraged to undertake roles capitalizing on their capabilities with knowledge, data, and manipulation.  Diplomat and Spy are ideal career choices for many Bothans, with Duties like Intelligence, Counter-Intelligence, and Political Support favoring their nature.  Most Alliance leaders understand that Bothans are likely to take whatever they learn and share it through the Spynet; these Rebels accept this as a fair price for gaining access to that same resources.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 1,
		agility: 2,
		intellect: 2,
		cunning: 3,
		willpower: 2,
		presence: 2
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 11,
			ability: 'willpower'
		}
	},
	baseXP: 100,
	bonuses: [
		{
			title: 'Special Ability',
			description: 'Bothans begin the game with one rank in Streetwise.  They still may not train Streetwise above rank 2 during character creation.  They also start with one rank in the Convincing Demeanor talent.'
		}
	]
};

module.exports = bothan;
