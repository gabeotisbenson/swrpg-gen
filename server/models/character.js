const mongoose	= require('mongoose');

const Schema = mongoose.Schema;

const talentSchema = require('./schemas/talent.js');

const skillCareer = {
	type: Boolean,
	required: true,
	default: false
};

const skillRank = {
	type: Number,
	required: true,
	validate: [val => val >= 0 && val <= 5, 'Rank must be between 0 and 5'],
	default: 0
};

const characterSchema = new Schema({
	'player': {
		'_id': {
			type: Schema.Types.ObjectId,
			required: true,
			index: true
		},
		'name': {
			type: String,
			required: true,
			index: true
		}
	},
	'name': {
		type: String,
		required: true,
		index: true
	},
	'species': {
		'_id': {
			type: Schema.Types.ObjectId,
			required: true
		},
		'name': {
			type: String,
			required: true
		}
	},
	'background': {
		type: String,
		required: true
	},
	'career': {
		'_id': {
			type: Schema.Types.ObjectId,
			required: true
		},
		'name': {
			type: String,
			required: true
		}
	},
	'specializations': {
		type: [
			{
				'_id': {
					type: Schema.Types.ObjectId,
					required: true
				},
				'name': {
					type: String,
					required: true
				}
			}
		],
		required: true
	},
	'attributes': {
		'soak': {
			type: Number,
			required: true
		},
		'wounds': {
			'threshold': {
				type: Number,
				required: true
			},
			'current': {
				type: Number,
				required: true,
				default: 0
			}
		},
		'strain': {
			'threshold': {
				type: Number,
				required: true
			},
			'current': {
				type: Number,
				required: true,
				default: 0
			}
		},
		'defense': {
			'ranged': {
				type: Number,
				required: true
			},
			'melee': {
				type: Number,
				required: true
			}
		}
	},
	'characteristics': {
		'brawn': {
			type: Number,
			required: true
		},
		'agility': {
			type: Number,
			required: true
		},
		'intellect': {
			type: Number,
			required: true
		},
		'cunning': {
			type: Number,
			required: true
		},
		'willpower': {
			type: Number,
			required: true
		},
		'presence': {
			type: Number,
			required: true
		}
	},
	'skills': {
		'general': {
			'astrogation': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'athletics': {
				'characteristic': { type: String, required: true, default: 'brawn' },
				'career': skillCareer,
				'rank': skillRank
			},
			'charm': {
				'characteristic': { type: String, required: true, default: 'presence' },
				'career': skillCareer,
				'rank': skillRank
			},
			'coercion': {
				'characteristic': { type: String, required: true, default: 'willpower' },
				'career': skillCareer,
				'rank': skillRank
			},
			'computers': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'cool': {
				'characteristic': { type: String, required: true, default: 'presence' },
				'career': skillCareer,
				'rank': skillRank
			},
			'coordination': {
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'deception': {
				'characteristic': { type: String, required: true, default: 'cunning' },
				'career': skillCareer,
				'rank': skillRank
			},
			'discipline': {
				'characteristic': { type: String, required: true, default: 'willpower' },
				'career': skillCareer,
				'rank': skillRank
			},
			'leadership': {
				'characteristic': { type: String, required: true, default: 'presence' },
				'career': skillCareer,
				'rank': skillRank
			},
			'mechanics': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'medicine': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'negotiation': {
				'characteristic': { type: String, required: true, default: 'presence' },
				'career': skillCareer,
				'rank': skillRank
			},
			'perception': {
				'characteristic': { type: String, required: true, default: 'cunning' },
				'career': skillCareer,
				'rank': skillRank
			},
			'pilotingPlanetary': {
				'name': { type: String, required: true, default: 'Piloting (Planetary)' },
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'pilotingSpace': {
				'name': { type: String, required: true, default: 'Piloting (Space)' },
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'resilience': {
				'characteristic': { type: String, required: true, default: 'brawn' },
				'career': skillCareer,
				'rank': skillRank
			},
			'skulduggery': {
				'characteristic': { type: String, required: true, default: 'cunning' },
				'career': skillCareer,
				'rank': skillRank
			},
			'stealth': {
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'streetwise': {
				'characteristic': { type: String, required: true, default: 'cunning' },
				'career': skillCareer,
				'rank': skillRank
			},
			'survival': {
				'characteristic': { type: String, required: true, default: 'cunning' },
				'career': skillCareer,
				'rank': skillRank
			},
			'vigilance': {
				'characteristic': { type: String, required: true, default: 'willpower' },
				'career': skillCareer,
				'rank': skillRank
			}
		},
		'combat': {
			'brawl': {
				'characteristic': { type: String, required: true, default: 'brawn' },
				'career': skillCareer,
				'rank': skillRank
			},
			'gunnery': {
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'lightsaber': {
				'characteristic': { type: String, required: true, default: 'brawn' },
				'career': skillCareer,
				'rank': skillRank
			},
			'melee': {
				'characteristic': { type: String, required: true, default: 'brawn' },
				'career': skillCareer,
				'rank': skillRank
			},
			'rangedLight': {
				'name': { type: String, required: true, default: 'Ranged (Light)' },
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			},
			'rangedHeavey': {
				'name': { type: String, required: true, default: 'Ranged (Heavy)' },
				'characteristic': { type: String, required: true, default: 'agility' },
				'career': skillCareer,
				'rank': skillRank
			}
		},
		'knowledge': {
			'coreWorlds': {
				'name': { type: String, required: true, default: 'Core Worlds' },
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'education': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'lore': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'outerRim': {
				'name': { type: String, required: true, default: 'Outer Rim' },
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'underworld': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'xenology': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			},
			'other': {
				'characteristic': { type: String, required: true, default: 'intellect' },
				'career': skillCareer,
				'rank': skillRank
			}
		},
		'custom': [
			{
				'name': {
					type: String,
					required: true
				},
				'characeristic': {
					type: String,
					required: true,
					validate: [val => ['brawn', 'agility', 'intellect', 'cunning', 'willpower', 'presence'].indexOf(val) !== -1, 'You must choose a valid characeristic']
				},
				'career': skillCareer,
				'rank': skillRank
			}
		]
	},
	'weapons': [
		{
			'_id': Schema.Types.ObjectId,
			'name': {
				type: String,
				required: true
			},
			'skill': {
				type: String,
				required: true
			},
			'damage': {
				type: Number,
				required: true
			},
			'range': {
				type: String,
				required: true
			},
			'crit': {
				type: Number,
				required: true
			},
			'special': String
		}
	],
	'xp': {
		'total': {
			type: Number,
			required: true,
			default: 0
		},
		'available': {
			type: Number,
			required: true,
			default: 0
		}
	},
	'gender': {
		type: String,
		required: true
	},
	'age': {
		type: String,
		required: true
	},
	'height': {
		type: String,
		required: true
	},
	'build': {
		type: String,
		required: true
	},
	'hair': {
		type: String,
		required: true
	},
	'eyes': {
		type: String,
		required: true
	},
	'features': {
		type: String,
		required: true
	},
	'motivations': [
		{
			'broad': {
				type: String,
				required: true
			},
			'specific': {
				type: String,
				required: true
			},
			'description': String
		}
	],
	'morality': {
		'value': {
			type: Number,
			required: true,
			default: 50,
			validate: [val => val >= 1 && val <= 100, 'Morality must be between 1 and 100']
		},
		'strength': {
			'name': {
				type: String,
				required: true
			},
			'description': {
				type: String,
				required: true
			}
		},
		'weakness': {
			'name': {
				type: String,
				required: true
			},
			'description': {
				type: String,
				required: true
			}
		},
		'conflict': {
			type: Number,
			required: true,
			default: 0,
			validate: [val => val >= 0, 'Cannot have negative conflict']
		}
	},
	'equipment': {
		'credits': {
			type: Number,
			required: true,
			default: 0
		},
		'tactical': [
			{
				'name': {
					type: String,
					required: true
				},
				'description': String
			}
		],
		'personal': [
			{
				'name': {
					type: String,
					required: true
				},
				'description': String
			}
		]
	},
	'injuries': [
		{
			'severity': {
				type: Number,
				required: true,
				validate: [val => val >= 1 && val <= 4, 'Severity must be between 1 and 4']
			},
			'result': {
				'name': {
					type: String,
					required: true
				},
				'description': {
					type: String,
					required: true
				}
			}
		}
	],
	'talents': [talentSchema],
	'forcePowers': [
		{
			'_id': {
				type: Schema.Types.ObjectId,
				required: true
			},
			'name': {
				type: String,
				required: true
			},
			'upgrades': [
				{
					'name': {
						type: String,
						required: true
					},
					'level': {
						type: Number,
						required: true
					}
				}
			],
			'mastered': {
				type: Boolean,
				required: true,
				default: false
			}
		}
	],
	'forceRating': Number,
	'notes': String
});

const Character = mongoose.model('Character', characterSchema);

module.exports = Character;
