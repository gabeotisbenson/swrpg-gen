const mongoose	= require('mongoose');

const Schema = mongoose.Schema;

const talentSchema = require('./schemas/talent.js');

const talentTreeSchema = new Schema({
	'talents': {
		type: [talentSchema],
		required: true
	}
});

const TalentTree = mongoose.model('TalentTree', talentTreeSchema);

module.exports = TalentTree;
