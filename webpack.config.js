const CleanWebpackPlugin	= require('clean-webpack-plugin');
const CompressionPlugin		= require('compression-webpack-plugin');
const HtmlWebpackPlugin		= require('html-webpack-plugin');
const path								= require('path');
const pkg									= require(path.join(__dirname, 'package'));
const webpack							= require('webpack');
const Visualizer					= require('webpack-visualizer-plugin');
const { VueLoaderPlugin }	= require('vue-loader');

const resolve = dir => path.join(__dirname, dir);

const commonConfig = {
	entry: path.join(__dirname, 'src', 'index.js'),
	devtool: 'source-map',
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
			title: pkg.name,
			template: path.join(__dirname, 'src', 'index.html')
		}),
		new VueLoaderPlugin(),
		new Visualizer()
	],
	resolve: {
		extensions: [
			'.js',
			'.vue',
			'.json'
		],
		alias: {
			'@src': resolve('src'),
			'@data': resolve('data')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							[
								'env',
								{
									targets: {
										browsers: [
											'last 2 versions',
											'ie >= 11'
										]
									}
								}
							]
						]
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			},
			{
				test: /\.styl(us)?$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'stylus-loader'
				]
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			}
		]
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	}
};

const devPatch = {
	resolve: {
		alias: {
			vue: 'vue/dist/vue'
		}
	}
};

const prodPatch = {
	resolve: {
		alias: {
			vue: 'vue/dist/vue.min'
		}
	}
};

const devConfig = Object.assign(devPatch, commonConfig);
const prodConfig = Object.assign(prodPatch, commonConfig);

prodConfig.plugins.push(new webpack.optimize.AggressiveMergingPlugin());
prodConfig.plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
prodConfig.plugins.push(new CompressionPlugin({
	asset: '[path].gz[query]',
	algorithm: 'gzip',
	test: /\.js$|\.css$|\.html$/,
	threshold: 10240,
	minRatio: 0
}));

module.exports = (env, argv) => {
	return argv['mode'] === 'production' ? prodConfig : devConfig;
};
