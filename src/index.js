import App from '@src/App.vue';
import router from '@src/router';
import Vue from 'vue';

import 'normalize.css';
import 'milligram/dist/milligram.css';

Vue.filter('capitalize', (val) => {
	if (!val) return '';
	val = val.toString();
	return val.charAt(0).toUpperCase() + val.slice(1);
});

const app = new Vue({
	el: '#app',
	router,
	render: h => h(App)
});
