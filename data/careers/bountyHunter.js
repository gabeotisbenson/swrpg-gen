const assassinTalentTree = require('@data/talentTrees');

const bountyHunter = {
	name: 'Bounty Hunter',
	game: 'Edge of the Empire',
	careerSkills: [
		'Athletics',
		'Brawl',
		'Perception',
		'Piloting (Planetary)',
		'Piloting (Space)',
		'Ranged (Heavy)',
		'Streetwise',
		'Vigilance'
	],
	description: {
		title: 'A Bounty Hunter\'s Role',
		sections: [
			'The name says it all: this is a hunter who seeks to collect bounties for his work.  Bounty Hunters tend to be feared, reviled, looked down upon--and in great demand throughout the galaxy.  They are expert trackers, brutal combatants, and effective investigators.  They tend to prefer skulduggery, stealth, and traps over frontal assaults, but the most respected Bounty Hunters will do whatever it takes to catch their quarries and collect their fees.',
			'Bounty Hunters generally fall within three categories: Imperial Bounty Hunters, Guild Bounty Hunters, or Independents.  Imperial Bounty Hunters are essentially on permanent contract to the Imperial Security Bureau (or a similar arm of Imperial law), and they never take assignments from corporations, smaller governments, or invididuals.  They are highly-trusted members of the extended law-enforcement community of the Empire, usually ex-military personnel that prefer a less-regimented (if possibly dangerous) way to serve.',
			'Those Bounty Hunters who join a guild enjoy many excellent benefits: room and board wherever the guild has a presence, exceptional training and access to high-end equipment as needed for specific assignments, and a steady stream of jobs given to them by the guild brokers acting on their behalf.  The downside to all of this is the fact that Guild Bounty Hunters have little-to-no say in what jobs they get, and failure to accept an assignment is grounds for immediate termination of a guild contract.',
			'The greatest freedom comes with being an Independent Bounty Hunter; it also comes with the greatest challenges and dangers.  Althought'
		]
	},
	specializations: {
		assassin: {
			name: 'Assassin',
			tagline: 'Instrument of Policy',
			careerSkills: [
				'Melee',
				'Ranged (Heavy)',
				'Skulduggery',
				'Stealth'
			],
			talentTree: assassinTalentTree
		},
		gadgeteer: {
			name: 'Gadgeteer',
			tagline: 'Connoisseur of Tech',
			careerSkills: [
				'Brawl',
				'Coercion',
				'Mechanics',
				'Ranged (Light)'
			]
		},
		survivalist: {
			name: 'Survivalist',
			tagline: 'Master of the Wild',
			careerSkills: [
				'Knowledge (Xenology)',
				'Perception',
				'Resilience',
				'Survival'
			]
		}
	},
	stories: {
		revenge: {
			name: 'Revenge'
		},
		disgrace: {
			name: 'Disgrace'
		},
		honor: {
			name: 'Honor'
		},
		aggression: {},
		legacy: {}
	}
};

module.exports = bountyHunter;
