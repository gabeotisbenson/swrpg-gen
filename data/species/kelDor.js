const kelDor = {
	name: 'Kel Dor',
	descriptions: [
		{
			game: 'Force and Destiny',
			text: 'Best known as a kindly and soft-spoken species, Kel Dors are most easily identified by the protective eyewear and rebreather masks that they must wear when outside of their native environments.  In spite of Kel Dors\' gentle natures, they are renowned for their sense of justice and willingness to enforce it with great prejudice.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Near-hairless humanoid mammals, Kel Dors average 1.7 meters in height.  Their respiratory systems are well adapted to Dorin\'s unusual atmosphwere of helium and unique gas compounds, so that they must make use of filtration masks in order to survive in other environments.  This, combined with innate toughness, does allow them to survive hard vacuum for a short time, an extraordinarily useful ability.',
						'Similarly, they wear protective and enhancing goggles because their black eyes are sensitized to the dim light and unusual atmospheric conditions of Dorin.  A highly developed extrasensory organ is located at the back of the Kel Dor skull, tightly linked to the brain.  Kel Dor skin tone ranges from a pale orange to a dark red.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Kel Dors are tightly linked into their extended families.  Several generations of a family often live together by choice.  This offers an opportunity to share responsibility for the extended family\'s younglings and ease in consulting the family\'s elders for advice.  Often, entire families share a common career, which can lead to family-owned businesses that span successive generations.',
						'Technological advancement drives Kel Dor society strongly.  Due to its own specialized needs, the species has become particularly adept at designing environmental systems.  These include individual and large-scale systems suited to a wide variety of environments.',
						'Justice is certain and swift on Dorin.  Security workers are quick to offer assistance to those in need, but they are also unwilling to wait long to see justice delivered instead, they interpret legal and moral issues in stark black and white terms, ignoring any potentially mitigating factors.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Dorin is located in the Expansion Region.  Due to its location between two black holes, it remained isolated from the rest of the galaxy until only a few centuries before the start of the galactic Civil War.  Dorin\'s atmosphere is composed largely of helium and a second unidentifiable gas that is unique to the system.  Dorin\'s oxygen levels are low enough that most non-natives require technological assistance to venture there.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Kel Dors learn both Kel Dorian and Basic from childhood.  They speak Basic as well as any native speaker and seldom speak Kel Dorian off-world.  Their native language is easier to speak and understand when they are breathing Dorin\'s native atmosphere.'
					]
				}
			]
		},
		{
			title: 'Perception of the Force',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'The Kel Dors have had a strong connection to the Force that predates their assocition with the greater galaxy.  For millennia, Sages of the obscure Baran Do tradition served as advisors to Kel Dor leaders.  Depending on sensory deprivation to offer a greater insight into the Force, these practitioners refined skills that were later practiced by the most talented of Jedi.  Whereas most Kel Dors have black eyes, those with the strongest connections to the Force frequently have silvery eyes, a fact that can be exceptionally dangerous for practitioners.  Since the rise of the Empire, more than a few Kel Dor Force-sensitives have been identified by their distinctive eyes and killed by Imperial agents.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 1,
		agility: 2,
		intellect: 2,
		cunning: 2,
		willpower: 3,
		presence: 2
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 10,
			ability: 'willpower'
		}
	},
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Kel Dors begin the game with one rank in Knowledge (Education).  They still may not train Knowledge (Education) above rank 2 during character creation.'
		},
		{
			title: 'Dark Vision',
			description: 'When making skill checks, Kel Dors remove up to [][] imposed due to darkness.'
		},
		{
			title: 'Atmospheric Requirement',
			description: 'Kel Dors must wear a specialized mask to breathe and see outside of their native atmosphere.  A Kel Dor character starts the game with an antitox breath mask and treats oxygen as a dangerous atmosphere with Rating 8 (see page 220).  However, Kel Dors may survive in vacuum for up to five minutes before suffering its effects.'
		}
	],
	extra: {
		baranDo: [
			'Thousands of years before Dorin became aware of the Galactic Republic, the Kel Dor had already discovered the power of the Force and crafted their own traditions.  A group of skilled and powerful mystics known as the Baran Do Sages studied the Force so that they could expand their abilities to predict danger for their people.  The Sages delved deeply into the ability to foresee events, whether those events were in the past, in the future, or in faraway lands.  Soon, they not only became an invaluable resource for criminal investigations, but also served as advisors to the Dorin\'s leadership.  Their predictive abilities helped Dorin avoid natural disasters, famines, diplomatic incidents, and even outright wars.',
			'With the coming of the Republic and the Jedi Order, the Baran Do\'s influence dwindled.  Many Force sensitives joined the Jedi, and thus the ranks of the Sages quickly shrank.  By the time of the Clone Wars, most Kel Dor had forgotten the Baran Do ever existed, and the few who know about them are unsure whether any Sages have passed on their traditions to the modern age.  This may be a boon in disguise, however.  As the Empire hunts down Force-sensitives across the galaxy, obscurity could prove the best defense.'
		]
	}
};

module.exports = kelDor;
