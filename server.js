// Required modules
const bodyParser	= require('body-parser');
const express			= require('express');
const path				= require('path');

// Turn on our app
const app = express();

// Body parsing
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Static files
app.use(express.static('dist'));

// Routes
const apiRoute = require(path.join(__dirname, 'server/routes/api'));
app.use('/api', apiRoute);

const port = 3000;
app.listen(port, () => {
	console.log('API listening on port ' + port);
});
