const twilek = {
	name: 'Twi\'lek',
	descriptions: [
		{
			game: 'Edge of the Empire',
			text: 'The Twi\'leks are among the most prominant non-human species in the galaxy.  They are expert bargainers, sly at reading other species and using cunning to get what they want.'
		},
		{
			game: 'Force and Destiny',
			text: 'Twi\'leks are a common sight throughout the galaxy, particularly within less reputable locales.  This is largely because the species has sold their young into slavery for many millennia.  They are easily recognized by their twin head-tails, also known as lekku or tchun-tchin.'
		}
	],
	information: [
		{
			title: 'Physiology',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Twi\'leks possess two perhensile tentacles, their tchun-tchin, or lekku, that project from the back of their heads.  These "head-tails" serve as a second tongue by which they can communicate through signs and gestures.  The rainbow of flesh tones found among the Twi\'leks are indicators of clan and region of ancestry.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Twi\'leks are an omnivorous, hairless, humanoid species.  Their lengthy lekku are prehensile but also serve additional functions.  These include playing a role in sensory input as well as providing a location for their brains to store memories.  Their smooth skin comes in a broad range of colors, which are commonly associated with ancestry and clan.  Some individuals even have natural striped patterns.  Their orange or yellow eyes offer exceptional night vision, and their flexible fingers terminate in clawlike nails.'
					]
				}
			]
		},
		{
			title: 'Society',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Family is everything in Twi\'lek society.  The government on Ryloth is an alliance of "head clans," each of which control a small town or larger districts in the city.  A family\'s five most important Twi\'leks lead their respected head-clans, with the power of influence radiating down the bloodline.  According to tradition, when one leader of the head clan dies, the four remaining members must take exile in the sun-baked Bright Lands where the vicious lyleks roam.  In practice, however, clan leaders find new and cunning ways to subvert exile.',
						'The clan system has stratified Twi\'lek society into castes.  Twi\'leks at the bottom of a bloodline are considered of the low birth caste and used as chattel in the slave trade.  Twi\'lek leaders exhibit disgust at any accusation of slavery and deny that they would ever put their own people in bondage, shifting the blame to other clans.  Nonetheless, nearly every clan engages in "contracted indenturehood."'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Twi\'lek society is tightly linked to an ancestral structure of clans.  In the lack of a central planetary government, each city is instead ruled by a head clan made up of five Twi\'lek males.  Each group of leaders is born to assume this role.  Notably, the head clan serves as a city\'s leadership until a member of the clan dies, at which point the surviving members are deposed so that the next generation can assume the leadership roles.  As these roles are hereditary, there are strong rivalries surrounding each appointment.',
						'Twi\'leks entered galactic society soon after the medicinal--and recreational--values of the mineral ryll were discovered.  At that time, Twi\'leks had no native spacefaring capability.  Partly because of this limitation, their world soon became dominated by the Hutts, who seized control of ryll mining and production.  In short order, this led to the Twi\'leks\' involvement in the slave trade.',
						'Twi\'leks are generally regarded as being cunning and manipulative, conducting subterfuge within their clans and in the service of other causes.  However, they are not particularly confrontational, preferring to avoid taking a stand on an issue whenever possible.'
					]
				}
			]
		},
		{
			title: 'Homeworld',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Though their home planet of Ryloth is located in the Outer Rim, Twi\'leks can be found on many worlds throughout the galaxy, due to their enterprising nature and the effects of slavery.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'Ryloth is an Outer Rim world located on the Corellian Run.  Its seasons are violent and its environments extreme, ranging from snowy wastelands to scorching deserts.  The majority of the native Twi\'leks live within a narrow temperate band.  For millennia, Ryloth\'s primary export has been slaves.  As a consequence, Twi\'leks can be found throughout the galaxy, and many have never visited the Twi\'lek homeworld.'
					]
				}
			]
		},
		{
			title: 'Language',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Ryl is the native tongue of Ryloth, yet there are few Twi\'leks who cannot speak Basic fluently.  What separates Twi\'leks from other species is their second, nonverabal language called Lekku, or "Twi\'leki."  In everyday conversation, Twi\'leks add texture and emphasis to their speech by moving and twitching their head-tails.  If privacy is necessary, Twi\'leks can converse in complete silence, using only the gestures of their head-tails to pass on complicated information.'
					]
				},
				{
					game: 'Force and Destiny',
					sections: [
						'The primary native spoken language of Twi\'leks is Ryl, and the vast majority are fluent in it.  In addition, Twi\'leks can communicate with one another using their lekku.  The head-tails can be used to accentuate spoken languages, but they may also be used in a nonspoken sign language.  Because of their galactic presence, most Twi\'leks also know Basic, while those who are active in the slave trade invariably learn Huttese.'
					]
				}
			]
		},
		{
			title: 'Life on the Fringe',
			descriptions: [
				{
					game: 'Edge of the Empire',
					sections: [
						'Given their linguistic talents, natural cunning, and social aptitude, Twi\'leks fit well into managerial, political, and leadership positions.  Lesser clan members unhappy with their status often leave Ryloth to strike out on their own, driving with an entreprenurial spirit to start their own businesses--or run their own crime syndicates.'
					]
				}
			]
		},
		{
			title: 'Perception of the Force',
			descriptions: [
				{
					game: 'Force and Destiny',
					sections: [
						'Twi\'leks do not profess any particular faith in the Force in a religious manner, nor do they have any native Force traditions.  However, conversations between Twi\'leks who use their lekku are belived to have spiritual connotations.  Some scholars of Jedi lore believe that the lekku have a degree of inherent Force sensitivity.  This suggests that Twi\'leks could have at least a limited connection to the Force, though they may not necessarily recognize it.'
					]
				}
			]
		}
	],
	abilities: {
		brawn: 1,
		agility: 2,
		intellect: 2,
		cunning: 2,
		willpower: 2,
		presence: 3
	},
	thresholds: {
		wound: {
			value: 10,
			ability: 'brawn'
		},
		strain: {
			value: 11,
			ability: 'willpower'
		}
	},
	baseXP: 100,
	bonuses: [
		{
			title: 'Special Abilities',
			description: 'Twi\'leks begin the game with one rank in either Charm or Deception.  They still may not train Charm or Deception above rank 2 during character creation.  When making skill checks, Twi\'leks may remove [] imposed due to arid or hot environmental conditions.'
		},
		{
			title: 'Desert Dwellers',
			description: 'When making skill checks, Twi\'leks may remove [] imposed due to arid or hot environmental conditions.'
		}
	],
	extra: {
		twilekEntertainers: 'Twi\'lek dance is regarded as one of the galaxy\'s most graceful adn sensual art forms.  Unfortunately, it has also led to many Twi\'leks (especially females) being sold as slave dancers.  However, some famous Twi\'lek warriors and scoundrels alike started their life as slaves before clawing their way into fame and glory.',
		twilekSlavery: 'Much of the galaxy finds Twi\'leks and slavery to be synonymous.  To many Twi\'leks\' shame, slavery is one of their homeworld\'s chief exports, and far too many of their fellows are complicit in this reprehensible business.  Many Twi\'leks are born into slavery.  Some become slaves far from Ryloth, while others are sold into slavery at a young age by their homeworld.  Under Imperial control, Twi\'lek slavery has become even more prevalent, as the Empire is perfectly happy to encourage the deplorable practice through neglect.  Even for those who escape, their time spent in captivity often defines their lives.  Some former slaves revel in their freedom, while others embark upon personal crusades to free others from such a thankless existence.'
	}
};

module.exports = twilek;
